# Pre Installation

1. Name resolution
vim /etc/hosts
10.80.80.10 mal-controller01
10.80.80.20 mal-controller02
10.80.80.30 mal-controller03
10.80.80.40 mal-compute01
10.80.80.50 mal-compute02
10.80.80.60 mal-compute03

2. Verify Connection
vim pod-list
mal-controller01
mal-controller02
mal-controller03
mal-compute01
mal-compute02
mal-compute03

for i in $(cat pod-list); do ping -c3 $i; done

3. Distribute controller01 pubkey to all nodes
## controller01
ssh-keygen -t rsa
cat .ssh/id_rsa.pub

## Copy to all nodes
vim .ssh/authorized_keys

4. Verify access
for i in $(cat pod-list); do ssh -o "StrictHostKeyChecking=accept-new" $i hostname; done

5. Update package
apt-get update

# Install Kolla Ansible
1. Install dependencies
apt-get install python3-dev libffi-dev gcc libssl-dev python3-selinux python3-setuptools python3-venv -y

2. Create a virtual environment and activate it
python3 -m venv kolla-venv
source kolla-venv/bin/activate

3. Install ansible and kolla-ansible
pip install -U pip
pip install ansible==2.9.20
pip install kolla-ansible==10.2.0

4. Create the /etc/kolla directory
mkdir -p /etc/kolla
chown $USER:$USER /etc/kolla

5. Copy globals.yml and passwords.yml to /etc/kolla directory
cp -r kolla-venv/share/kolla-ansible/etc_examples/kolla/* /etc/kolla

6. Copy all-in-one and multinode inventory files to the current directory
cp kolla-venv/share/kolla-ansible/ansible/inventory/* .

7. Configure multinode inventory.
vim multinode

[control]
mal-controller[01:03]

[network]
mal-controller[01:03]

[compute]
mal-compute[01:03]

[monitoring]
mal-controller[01:03]

[storage]
mal-controller[01:03]

[deployment]
localhost       ansible_connection=local

8. Configure ansible
mkdir -p /etc/ansible
vim /etc/ansible/ansible.cfg

[defaults]
host_key_checking=False
pipelining=True
forks=100

9. Verify connection using ansible ping module
ansible -i multinode all -m ping

10. Generate kolla password
kolla-genpwd
cat /etc/kolla/passwords.yml

11. Configure your openstack cluster on kolla globals.yml
vim /etc/kolla/globals.yml

kolla_base_distro: "ubuntu"
kolla_install_type: "source"
openstack_release: "ussuri"
kolla_internal_vip_address: "10.80.80.100"
network_interface: "ens3"
neutron_external_interface: "ens4"
enable_cinder: "yes"
glance_backend_ceph: "yes"
cinder_backend_ceph: "yes"
nova_backend_ceph: "yes"

12. Verify globals.yml configuration
cat /etc/kolla/globals.yml | grep -v "#" |  tr -s [:space:]

13. Bootstrap servers
kolla-ansible -i ./multinode bootstrap-servers

# Deploy Ceph using cephadm
1. Download and install cephadm
curl --silent --remote-name --location https://github.com/ceph/ceph/raw/octopus/src/cephadm/cephadm
chmod +x cephadm

2. Add ceph repository
echo deb https://download.ceph.com/debian-octopus/ $(lsb_release -sc) main | sudo tee /etc/apt/sources.list.d/ceph.list
wget -q -O- 'https://download.ceph.com/keys/release.asc' | sudo apt-key add -
apt update

3. Install cephadm
./cephadm install

4. Bootstrap ceph
mkdir -p /etc/ceph
cephadm bootstrap \
--mon-ip 10.80.80.10 \
--initial-dashboard-user admin \
--initial-dashboard-password Str0ngAdminP@ssw0rd

5. Distribute ceph public key to all nodes
for i in $(cat pod-list); do ssh-copy-id -f -i /etc/ceph/ceph.pub root@$i; done

6. Install ceph client
apt install -y ceph-common

7. Add node orchestration
for i in $(cat pod-list); do ceph orch host add $i; done

8. Verify host
ceph orch host ls

9. Add label to nodes
ceph orch host label add mal-controller01 mgr
ceph orch host label add mal-controller01 mon
ceph orch host label add mal-controller02 mgr
ceph orch host label add mal-controller02 mon
ceph orch host label add mal-controller03 mgr
ceph orch host label add mal-controller03 mon

ceph orch host label add mal-compute01 osd
ceph orch host label add mal-compute02 osd
ceph orch host label add mal-compute03 osd

10. Verify host
ceph orch host ls

11. Create cluster configuration
vim ceph_cluster_conf.yaml

service_type: mon
placement:
  count: 3
  label: mon
---
service_type: mgr
placement:
  count: 3
  label: mgr
---
service_type: osd
service_id: osd_spec_default
placement:
  label: osd
data_devices:
  size: '30G'

12. Apply configuration
ceph orch apply -i ceph_cluster_conf.yaml

13. Monitor ceph status until all osd up and in
watch ceph -s

## If you want to disable stray warning
ceph config set mgr mgr/cephadm/warn_on_stray_daemons false

# Ceph verification
1. Ceph health
ceph health detail

2. Raw and pool storage
ceph df

3. Ceph status
ceph -s

4. OSD status
ceph osd stat

5. OSD detail
ceph osd dump

6. OSD tree
ceph osd tree

7. Mon status
ceph mon stat

8. Mon detail
ceph mon dump

9. Quorum status
ceph quorum_status

10. Auth list
ceph auth list

# Managing Pool
1. Create pool named pool-test with 2 pg
ceph osd pool create <pool-name> <pg-num>
ceph osd pool create pool-test 100
ceph osd lspools

2. Associate pool to application (choose 1)
ceph osd pool application enable <pool-name> cephfs
ceph osd pool application enable pool-test cephfs

ceph osd pool application enable <pool-name> rbd
ceph osd pool application enable pool-test rbd

ceph osd pool application enable <pool-name> rgw
ceph osd pool application enable pool-test rgw

ceph osd pool application get pool-test

3. Intialize pool
rbd pool init pool-test
echo test > file-test

4. I/O information
ceph osd pool stats pool-test

5. Create object data
rados put {object-name} {file-name} --pool=[pool-name]
rados put object-test file-test --pool=pool-test

rados ls --pool={pool-name}
rados ls --pool=pool-test

6. Delete object data
rados rm {object-name} --pool={pool-test}
rados rm object-test --pool=pool-test
rados ls --pool=pool-test

7. Configuration to allow deleting pool
ceph tell mon.* injectargs --mon-allow-pool-delete=true
ceph daemon /var/run/ceph/0ff2a73a-c44b-11ec-9634-52540036d8e6/ceph-mon.mal-controller01.asok config show | grep mon_allow_pool_delete

8. Disable OSD pool application
ceph osd pool application disable pool-test rbd --yes-i-really-mean-it
ceph osd pool application get pool-test

9. Delete pool
ceph osd pool delete pool-test pool-test --yes-i-really-really-mean-it
ceph osd lspools

# Ceph Block Storage
1. Create pool
ceph osd pool create pool-test2 128
rbd pool init pool-test2
rbd create pool-test2/block0 --size 5120

2. Check block rbd
rbd list pool-test2
rbd info pool-test2/block0

3. Maping image to block device
rbd map pool-test2/block0 -m mal-controller01 -k /etc/ceph/ceph.client.admin.keyring
rbd showmapped

4. Format filesystem
mkfs.xfs /dev/rbd0

5. Mount block device
mkdir /mnt/block0
mount /dev/rbd0 /mnt/block0
df -h

# Ceph Snapshot and Clone
1. Create Snapshot
fallocate -l 1G /mnt/block0/test1.img
fallocate -l 1G /mnt/block0/test2.img
fallocate -l 1G /mnt/block0/test3.img
ls -lh /mnt/block0
rbd snap create pool-test2/block0@snap-block0

2. Show Snapshot
rbd snap ls pool-test2/block0

3. Test Snapshot
rm -rf /mnt/block0/test3.img
ls -lh /mnt/block0/
umount /dev/rbd0
df -h
rbd unmap pool-test2/block0

rbd snap rollback pool-test2/block0@snap-block0
rbd map pool-test2/block0
mount /dev/rbd0 /mnt/block0
ls -lh /mnt/block0/

4. Protect the Snapshot
rbd info pool-test2/block0@snap-block0
rbd snap protect pool-test2/block0@snap-block0
rbd info pool-test2/block0@snap-block0

5. Create clone
rbd clone pool-test2/block0@snap-block0 pool-test2/clone-block0
rbd info pool-test2/clone-block0

6. Show snapshot children (clone)
rbd children pool-test2/block0@snap-block0
rbd info pool-test2/clone-block0

7. Flattend Cloned Image
rbd flatten pool-test2/clone-block0
rbd info pool-test2/clone-block0
rbd list pool-test2

8. Unprotect snapshot
rbd info pool-test2/block0@snap-block0
rbd snap rm pool-test2/block0@snap-block0
rbd snap unprotect pool-test2/block0@snap-block0
rbd info pool-test2/block0@snap-block0

9. Delete snapshot
rbd snap ls pool-test2/block0
rbd snap rm pool-test2/block0@snap-block0
rbd snap ls pool-test2/block0

10. Purge/Delete all snapshots
rbd snap purge pool-test2/block0
ceph osd pool delete pool-test2 pool-test2 --yes-i-really-really-mean-it


# Ceph Object Storage
1. Create radosgateway
ceph orch apply rgw --realm_name=myorg --zone_name=id-jkt-1 --port=8888 --placement="1 mal-controller01"

2. Watch orch ls until not shown unknown
watch "ceph orch ls | grep id-jkt-1"

3. Install awscli
apt install -y awscli

4. List user
radosgw-admin user list

5. Create user
radosgw-admin user create --uid=user0 --display-name=user0

6. Save the key from output when creating user
"keys": [
        {
            "user": "user0",
            "access_key": "NKTHQMADPSGT20TEPYQ6",
            "secret_key": "g5XM2EJBgaf3SVK2TWpCb4lfEzO2APCJIl4840bd"
        }
    ]

7. Verify user
radosgw-admin user list

8. Configure aws client user
aws configure --profile=user0

AWS Access Key ID [None]: I2BS5OV91EXHEQ9HJ81F
AWS Secret Access Key [None]: HesLszdZB1v3aDEYwzd1PtHHdesBMGIZq0GlRVt6
Default region name [None]: id-jkt-1
Default output format [None]: json

9. Create bucket
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3api create-bucket --bucket bucket0
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3api create-bucket --bucket bucket1
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3api create-bucket --bucket bucket2
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 ls

10. Add file to bucket
echo "file0" > file0.txt
echo "file1" > file1.txt
echo "file2" > file2.txt

aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 cp file0.txt s3://bucket0
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 cp file1.txt s3://bucket1
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 cp file2.txt s3://bucket2

aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 ls s3://bucket0
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 ls s3://bucket1
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 ls s3://bucket2

11. Remove file
rm file0.txt file1.txt file2.txt
ls

12. Download file from bucket
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 cp s3://bucket0/file0.txt .
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 cp s3://bucket1/file1.txt .
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 cp s3://bucket2/file2.txt .

ls file*

13. Remove bucket
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 rb s3://bucket0 --force
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 rb s3://bucket1 --force
aws --profile user0 --endpoint-url http://mal-controller01:8080/ s3 rb s3://bucket2 --force

# Ceph Dashboard
1. Create user ceph dashboard
ceph dashboard ac-user-create <username> <password> <role>
echo adminpass > adminpass
ceph dashboard ac-user-create admin administrator -i adminpass
ceph mgr services

2. Akses from browser
https://mal-controller01:8443/

# Setup Ceph Config File
1. Save ceph config
cephadm shell cat /etc/ceph/ceph.conf > /etc/ceph/ceph.conf

2. Remove indentation in ceph config
vim /etc/ceph/ceph.conf

[global]
fsid = 0ff2a73a-c44b-11ec-9634-52540036d8e6
mon_host = [v2:10.80.80.10:3300/0,v1:10.80.80.10:6789/0] [v2:10.80.80.20:3300/0,v1:10.80.80.20:6789/0] [v2:10.80.80.30:3300/0,v1:10.80.80.30:6789/0]

# Creating Pool for Openstack Storage
1. Create pool
ceph osd pool create volumes
ceph osd pool create images
ceph osd pool create backups
ceph osd pool create vms

rbd pool init volumes
rbd pool init images
rbd pool init backups
rbd pool init vms

2. Verify pool
ceph osd lspools

# Distribute Ceph configuration to all Controller nodes
ssh mal-controller02 mkdir -p /etc/ceph
ssh mal-controller03 mkdir -p /etc/ceph

cat /etc/ceph/ceph.conf | ssh mal-controller02 tee /etc/ceph/ceph.conf
cat /etc/ceph/ceph.conf | ssh mal-controller03 tee /etc/ceph/ceph.conf

# Setup Keyring Openstack
1. Create keyring for openstack
ceph auth get-or-create client.glance mon 'profile rbd' osd 'profile rbd pool=images' mgr 'profile rbd pool=images'
ceph auth get-or-create client.cinder mon 'profile rbd' osd 'profile rbd pool=volumes, profile rbd pool=vms, profile rbd-read-only pool=images' mgr 'profile rbd pool=volumes, profile rbd pool=vms'
ceph auth get-or-create client.cinder-backup mon 'profile rbd' osd 'profile rbd pool=backups' mgr 'profile rbd pool=backups'

2. Copy keyring to mal-controller01
ceph auth get-or-create client.glance | ssh mal-controller01 sudo tee /etc/ceph/ceph.client.glance.keyring
ceph auth get-or-create client.cinder | ssh mal-controller01 sudo tee /etc/ceph/ceph.client.cinder.keyring
ceph auth get-or-create client.cinder-backup | ssh mal-controller01 sudo tee /etc/ceph/ceph.client.cinder-backup.keyring

3. Copy keyring to mal-controller02
ceph auth get-or-create client.glance | ssh mal-controller02 sudo tee /etc/ceph/ceph.client.glance.keyring
ceph auth get-or-create client.cinder | ssh mal-controller02 sudo tee /etc/ceph/ceph.client.cinder.keyring
ceph auth get-or-create client.cinder-backup | ssh mal-controller02 sudo tee /etc/ceph/ceph.client.cinder-backup.keyring

4. Copy keyring to mal-controller03
ceph auth get-or-create client.glance | ssh mal-controller03 sudo tee /etc/ceph/ceph.client.glance.keyring
ceph auth get-or-create client.cinder | ssh mal-controller03 sudo tee /etc/ceph/ceph.client.cinder.keyring
ceph auth get-or-create client.cinder-backup | ssh mal-controller03 sudo tee /etc/ceph/ceph.client.cinder-backup.keyring

# Add Keyring for Glance
1. Add ceph.conf and keyring for glance
mkdir -p /etc/kolla/config/glance/
cp /etc/ceph/ceph.conf /etc/kolla/config/glance/
cp /etc/ceph/ceph.client.glance.keyring /etc/kolla/config/glance/

# Add Keyring for Cinder
1. Add ceph.conf and keyring for cinder
mkdir -p /etc/kolla/config/cinder/cinder-volume
mkdir /etc/kolla/config/cinder/cinder-backup
cp /etc/ceph/ceph.client.cinder.keyring /etc/kolla/config/cinder/cinder-volume/
cp /etc/ceph/ceph.client.cinder.keyring /etc/kolla/config/cinder/cinder-backup/
cp /etc/ceph/ceph.client.cinder-backup.keyring /etc/kolla/config/cinder/cinder-backup/

# Setup ceph.conf File to Integrate with Openstack
1. Add ceph.conf to glance
cp /etc/ceph/ceph.conf /etc/kolla/config/glance/

2. Add ceph.conf to cinder-volume
cp /etc/ceph/ceph.conf /etc/kolla/config/cinder/cinder-volume/
cp /etc/ceph/ceph.conf /etc/kolla/config/cinder/cinder-backup/

3. Add ceph.conf and keyring for nova
mkdir /etc/kolla/config/nova
cp /etc/ceph/ceph.conf /etc/kolla/config/nova/
cp /etc/ceph/ceph.client.cinder.keyring /etc/kolla/config/nova/
cp /etc/ceph/ceph.client.cinder.keyring /etc/kolla/config/nova/ceph.client.nova.keyring

# Tuning Glance, Cinder, Nova
1. Glance configuration tuning
vim /etc/kolla/config/glance/glance-api.conf

[glance_store]
stores = rbd
default_store = rbd
rbd_store_pool = images
rbd_store_user = glance
rbd_store_ceph_conf = /etc/ceph/ceph.conf

2. Cinder configuration tuning
cat /etc/kolla/passwords.yml |grep cinder_rbd_secret_uuid
vim /etc/kolla/config/cinder.conf

[DEFAULT]
enabled_backends=rbd-1
backup_ceph_conf=/etc/ceph/ceph.conf
backup_ceph_user=cinder-backup
backup_ceph_chunk_size = 134217728
backup_ceph_pool=backups
backup_driver = cinder.backup.drivers.ceph.CephBackupDriver
backup_ceph_stripe_unit = 0
backup_ceph_stripe_count = 0
restore_discard_excess_bytes = true

[rbd-1]
rbd_ceph_conf=/etc/ceph/ceph.conf
rbd_user=cinder
backend_host=rbd:volumes
rbd_pool=volumes
volume_backend_name=rbd-1
volume_driver=cinder.volume.drivers.rbd.RBDDriver
rbd_secret_uuid = 25751f9e-d341-48f9-b5d2-ac7b3f2a760c

3. Nova configuration tuning
vim /etc/kolla/config/nova.conf

[libvirt]
images_rbd_pool=vms
images_type=rbd
images_rbd_ceph_conf=/etc/ceph/ceph.conf
rbd_user=cinder

# Deploy Openstack with Kolla-Ansible
1. Precheck
kolla-ansible -i multinode prechecks

2. Deploy OpenStack
kolla-ansible -i multinode deploy

3. Post deploy
kolla-ansible -i multinode post-deploy

4. Install openstack client
pip3 install python-openstackclient
source /etc/kolla/admin-openrc.sh

5. Create volume type with rbd support
openstack volume type create rbd-1

# Creating Image Openstack
1. Download cirros image
wget http://download.cirros-cloud.net/0.5.1/cirros-0.5.1-x86_64-disk.img

2. Create cirros image 
openstack image create --disk-format qcow2 \
--container-format bare --public \
--file cirros-0.5.1-x86_64-disk.img cirros

3. Show image list 
openstack image list

4. Show image details
openstack image show cirros

5. Show image list in ceph
rbd ls images

6. Show image details in ceph
rbd info images/image_id

# Creating External Network

1. Create external network named external-net-8
cat /etc/kolla/neutron-server/ml2_conf.ini | grep flat_network

openstack network create --share --external \
--provider-physical-network physnet1 \
--external-net-8work-type flat external-net-8

2. Create subnet for external network named external-subnet-8
openstack subnet create --network external-net-8 \
--gateway 10.81.81.1 --no-dhcp \
--subnet-range 10.81.81.0/24 external-subnet-8

3. Show network and subnet list
openstack network list
openstack subnet list

4. Show network and subnet details
openstack network show external-net-8
openstack subnet show external-subnet-8

# Creating Internal Network
1. Create internal network named internal-net-8
openstack network create internal-net-8

2. Create subnet for internal-subnet-8
openstack subnet create --network internal-net-8  \
  --allocation-pool start=192.168.88.10,end=192.168.88.254 \
  --dns-nameserver 8.8.8.8 --gateway  192.168.88.1 \
  --subnet-range 192.168.88.0/24 internal-subnet-8

3. Show network and subnet list
openstack network list
openstack subnet list

4. Show network and subnet details
openstack network show internal-net-8
openstack subnet show internal-subnet-8

# Creating Router named router-8
1. Create Router
openstack router create router-8

2. Set router external gateway to external-net-8
openstack router set --external-gateway external-net-8 router-8

3. Add subnet internal-subnet to router-8
openstack router add subnet router-8 internal-subnet-8

4. Show router list
openstack router list

5. Show router details
openstack router show router-8

# Creating Security Group
1. Create security group
openstack security group create allow-icmp-ssh --description 'Allow SSH and ICMP'

2. Add rule icmp and ssh
openstack security group rule create --protocol icmp allow-icmp-ssh
openstack security group rule create --protocol tcp --ingress --dst-port 22 allow-icmp-ssh

3. Show security group list
openstack security group list

4. Show security group detail
openstack security group show allow-icmp-ssh

# Creating Keypair
1. Create keypair using controller01 pubkey
openstack keypair create --public-key ~/.ssh/id_rsa.pub controller-key

2. Show keypair list
openstack keypair list

3. Show keypair details
openstack keypair show controller-key

# Creating Flavor
1. Create flavor named small with 1 vcpu, 512 MB RAM, and 5 GB disk
openstack flavor create --ram 512 --disk 5 --vcpus 1 --public small

2. Show flavor list
openstack flavor list

3. Show flavor details
openstack flavor show small

# Creating Instance
1. Create instance named cirros-instance
openstack server create --flavor small \
--image cirros \
--key-name controller-key \
--security-group allow-icmp-ssh \
--network internal-net-8 \
cirros-instance

2. Show instance list 
openstack server list

3. Show instance details
openstack server show cirros-instance

4. Show instance list from ceph
rbd ls vms

# Creating Floating IP
1. Create floating ip
openstack floating ip create --floating-ip-address 10.81.81.100 external-net-8

2. Add floating ip to cirros-instance
openstack server add floating ip cirros-instance 10.81.81.100

3. Show instance list
openstack server list

4. Ping to floating ip
ping -c3 10.81.81.100

5. SSH to cirros-instance via floating ip
ssh cirros@10.81.81.100

6. Ping google.com from instance
ping -c3 google.com

# Integrating Ceph RGW and Openstack Object Storage

1. Install swift client
pip install python-swiftclient

2. Create swift service
openstack service create --name=swift --description="Swift Service" object-store
openstack service list

3. Create swift endpoint
openstack endpoint create --region RegionOne object-store public "http://10.90.90.10:8888/swift/v1"
openstack endpoint create --region RegionOne object-store internal "http://10.90.90.10:8888/swift/v1"
openstack endpoint create --region RegionOne object-store admin "http://10.90.90.10:8888/swift/v1"
openstack endpoint list |grep swift

4. Show rgw service name
ceph orch ps pod9-controller01

5. Show rgw container volume mapping
docker ps |grep rgw
docker inspect <container_id> |grep /etc/ceph/ceph.conf

6. Edit ceph.conf for rgw container
vim /var/lib/ceph/6489893c-c8ef-11ec-990d-525400a2dc7d/rgw.myorg.id-jkt-1.pod9-controller01.fvddgg/config

...
[client.rgw.myorg.id-jkt-1.pod9-controller01.fvddgg]
host=pod9-controller01
rgw keystone api version = 3
rgw keystone url = http://10.90.90.100:5000
rgw keystone admin user = admin
rgw keystone admin password = ONK6v8XSOqHCyp2fxocQzCCfBBGtxpM31r9KmkMs
rgw keystone admin tenant = admin
rgw keystone admin domain = default
...

7. Restart rgw service
systemctl -t service |grep rgw
systemctl restart ceph-6489893c-c8ef-11ec-990d-525400a2dc7d@rgw.myorg.id-jkt-1.pod9-controller01.fvddgg.service

8. Show swift detail
swift stat

9. Create container
swift post container-test1
swift list

10. Create files
echo "test-file1" > file1.txt
echo "test-file2" > file2.txt
echo "test-file3" > file3.txt

11. Upload files to container
swift upload container-test1 file1.txt
swift upload container-test1 file2.txt
swift upload container-test1 file3.txt

12. List object in container
swift list container-test1

13. Delete local files
rm -rf file*.txt

14. Download files from container
swift download container-test1

15. Show files content
cat file*.txt

# Add New Compute
1. Add new compute hostname to /etc/hosts in all nodes
10.90.90.70 pod9-compute04

2. Copy controller01 pubkey to new compute
### controller01
cat .ssh/id_rsa.pub

### new compute
vim .ssh/authorized_keys

3. Verify by ssh
ssh root@pod9-compute04 hostname

4.  Update package
apt-get update

5. Configure multinode inventory.
vim multinode

[compute]
pod9-compute04

6. Verify connection by ping
ansible -m ping all -i multinode --limit pod9-compute04

7. Deploy kolla-ansible with limit parameter
kolla-ansible -i multinode bootstrap-servers --limit pod9-compute04
kolla-ansible -i multinode prechecks --limit pod9-compute04
kolla-ansible -i multinode deploy --limit pod9-compute04

8. Show compute service list
openstack compute service list

# Add New OSD
### controller01
1. Copy ceph pubkey to new compute
ssh-copy-id -f -i /etc/ceph/ceph.pub root@pod9-compute04

2. Add node orchestration and label

ceph orch host add pod9-compute04
ceph orch host label add pod9-compute04 osd

3. Add daemon
ceph orch device ls
ceph orch daemon add osd pod9-compute04:/dev/vdb
ceph orch daemon add osd pod9-compute04:/dev/vdc

4. Monitor ceph status
watch -n1 ceph -s

# Take out and Remove OSD
1. Show OSD tree
ceph osd tree

2. Remove daemon
ceph orch daemon rm osd.1 --force

3. Show ceph status
ceph -s

4. Take out and remove OSD
ceph osd out osd.1
ceph osd crush reweight osd.1 0
ceph osd crush remove osd.1
ceph osd rm osd.1

5. Show ceph status
ceph -s
